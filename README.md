# Desafiocris

Desafios Mandic   

Documentação Desafio1

**Realizar a Criação da Maquina AWS**
Modelo de EC2 t3a.medium location    us-east-1a

- Realizar a conexão via  SSH Example:
ssh -i "Arquivo.pem" "Maquina".compute-1.amazonaws.com
- Atualizar máquina comando yum update


**Criar Dominio**
- Realizar cadastro e criar dominio em :http://www.dot.tk/pt/index.html

Resultato esperado: http://desafiocris.tk/

**Criar banco de dados**
- Seguir Documentação: https://linuxize.com/post/install-mariadb-on-centos-7/


**Colocar Certificado SSL**
- Seguir Documentação: https://certbot.eff.org/lets-encrypt/centosrhel7-nginx

Resultado esperado: https://blog.desafiocris.tk/


**Instalação nginx e Wordpress**
- Seguir Documentação: https://linuxize.com/post/how-to-install-wordpress-with-nginx-on-centos-7/

Resultado esperado: https://blog.desafiocris.tk/


**Instalação Magento:**
- Seguir Documentação: https://devdocs.magento.com/guides/v2.4/install-gde/prereq/nginx.html#centos-7

Resultado esperado: http://loja-blog.desafiocris.tk/

Observação: alguns diretórios é necessário alterar as permições em: chown -R nginx:nginx /var/lib/php e chown -R nginx:nginx /var/www/html/

**Instalação Tomcat**

- Seguir Documentação: https://www.youtube.com/watch?v=m21nFreFw8A

Observação: Arquivo configurado: 

server {
        listen 80;
        listen [::]:80;
        server_name tomcat.desafiocris.tk;



   location / {
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass http://127.0.0.1:8080/;
  }
}

Resultado esperado: http://tomcat.desafiocris.tk/

**Site estático:**
- Documentação: https://blog.zerokol.com/2017/12/configurando-o-nginx-para-servir-um.html

Resultado esperado: http://site.desafiocris.tk/





